"""
Maakt een script dat met de NXAPI van beide Nexus switches
data ophaalt (serienummer, etc) en aanmaakt in Nautobot
"""
import json
import requests
import nautobot_tools

USERNAME = "cisco"
PASSWORD = "cisco"

SITE = "devnet_sandbox"
DEVICE_TYPE = "nexus9000v"
DEVICE_ROLE = "switch"
PLATFORM = "cisco_nxos"

devices = ["10.10.20.177","10.10.20.178"]

# get device info IDs
site_id = nautobot_tools.name_to_id("dcim/sites/", SITE)
device_type_id = nautobot_tools.name_to_id("dcim/device-types/", DEVICE_TYPE)
device_role_id = nautobot_tools.name_to_id("dcim/device-roles/", DEVICE_ROLE)
platform_id = nautobot_tools.name_to_id("dcim/platforms/", PLATFORM)

for device in devices:
    url=f'http://{device}/ins'
    headers={'content-type':'application/json-rpc'}
    payload=[
    {
        "jsonrpc": "2.0",
        "method": "cli",
        "params": {
            "cmd": "show version",
            "version": 1
        },
        "id": 1
    }
    ]
    response = requests.post(
        url,data=json.dumps(payload), headers=headers,auth=(USERNAME,PASSWORD), verify=False
        ).json()

    serial = response['result']['body']['proc_board_id']
    hostname = response['result']['body']['host_name']
    os_version = response['result']['body']['kickstart_ver_str']
    print(f"{hostname} {serial} {os_version}")

    # create device
    device_dict = {
        "name": hostname,
        "device_type": device_type_id,
        "device_role": device_role_id,
        "platform": platform_id,
        "site": site_id,
        "serial": serial,
        "comments": os_version,
        "status": "active",
    }

    device_id = nautobot_tools.create_device(device_dict)

    # create management interface
    INTERFACE_NAME = "mgmt0"
    interface_id = nautobot_tools.create_interface(device_id, INTERFACE_NAME)

    # create IP address
    IP_ADDR = device + "/24"
    ip_id = nautobot_tools.create_ipaddress(IP_ADDR,interface_id)

    # make_primary
    json_data = {"primary_ip4": ip_id}
    nautobot_tools.update_device(device_id, json_data)
