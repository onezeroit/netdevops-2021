"""
Nautobot toolbox functies
"""
import json
import requests

# Nautobot settings
TOKEN = "f25ca399a4f7d0da32d53b7cbeebe3d643a7fb74"
URL = "http://10.10.20.50:8000/api/"

def get_inventory():
    """
    haal inventory data op uit Nautobot (geformateerd zoals Netmiko het wil hebben)
    """
    query = URL + "dcim/devices/"
    headers = {
        "Authorization": "Token "+TOKEN,
    }
    response = requests.get(query, headers=headers)

    device_list = json.loads(response.text)
    results = []
    for device in device_list["results"]:
        node = {
            "host": device["primary_ip4"]["address"].split("/")[0],
            "device_type": device["platform"]["slug"]
        }
        results.append(node)

    return results

def create_device(device_dict):
    """
    maakt device aan in Nautobot
    returns device_id
    """
    query = URL + "dcim/devices/"
    headers = {
        "Authorization": "Token "+TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    response = requests.post(query, headers=headers, json=device_dict)
    device_id = json.loads(response.text)['id']
    return device_id

def create_interface(device_id, interface):
    """
    maakt interface aan in Nautobot voor device
    returns interface_id
    """
    query = URL + "dcim/interfaces/"
    headers = {
        "Authorization": "Token "+TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    json_data = {
        "device": device_id,
        "name": interface,
        "type": "1000base-t"
    }
    response = requests.post(query, headers=headers, json=json_data)
    return json.loads(response.text)['id']

def create_ipaddress(ip_address, interface):
    """
    maakt ipaddress aan in Nautobot, en koppelt aan interface
    returns ip_id
    """
    query = URL + "ipam/ip-addresses/"
    headers = {
        "Authorization": "Token "+TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    json_data = {
        "address": ip_address,
        "status": "active",
        "assigned_object_type": "dcim.interface",
        "assigned_object_id": interface
    }
    response = requests.post(query, headers=headers, json=json_data)
    return json.loads(response.text)['id']

def update_device(device_id,data):
    """
    Patch device met 'data' dict.
    """
    query = URL + "dcim/devices/" + device_id + "/"
    headers = {
        "Authorization": "Token "+TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    response = requests.patch(query, headers=headers, json=data)
    return response.status_code


def name_to_id(api, name):
    """
    resolve naam/slug naar uuid
    """
    query = f"{URL}{api}?slug={name}"
    headers = {
        "Authorization": "Token "+TOKEN,
    }
    response = requests.get(query, headers=headers)
    result = json.loads(response.text)
    if result['count'] == 1:
        uuid = result["results"][0]["id"]
    else:
        uuid = False
    return uuid

if __name__ == "__main__":
    API = "dcim/sites/"
    NAME = "devnet_sandbox"
    output = name_to_id(API,NAME)
    print(output)
