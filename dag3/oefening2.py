"""
Maak een script dat met Netmiko een config backup maakt
Haal hiervoor al inventory een ‘device lijst’ uit Nautobot

"""
from datetime import date
from netmiko import Netmiko
from nautobot_tools import get_inventory

# inventory
USERNAME = "cisco"
PASSWORD = "cisco"

device_list = get_inventory()

# set date string
TODAY = str(date.today())

# backup command
COMMAND = "show run"

# loop over routers
for router in device_list:
    # add credentials
    router["username"] = USERNAME
    router["password"] = PASSWORD

    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    output = net_connect.send_command(COMMAND)
    # disconnect
    net_connect.disconnect()

    # save config
    filename = router['host']+"-"+TODAY+".config"
    print(router['host']+": saving config to "+filename)
    with open(filename, "w", encoding="utf8") as output_file:
        output_file.write(output)
