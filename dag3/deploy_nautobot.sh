#!/bin/bash

# install netbox/docker
docker run -itd --name nautobot -p 8000:8000 networktocode/nautobot-lab

# add site
docker exec -it nautobot nautobot-server nbshell --command 'Site(name="DevNet Sandbox",slug="devnet_sandbox", status=Status.objects.get(slug="active")).validated_save()'
# add prefix
docker exec -it nautobot nautobot-server nbshell --command 'Prefix(prefix="10.10.20.0/24",description="Management Network", site=Site.objects.get(slug="devnet_sandbox"),status=Status.objects.get(slug="active")).validated_save()'

# device types
docker exec -it nautobot nautobot-server nbshell --command 'Manufacturer(name="Cisco", slug="cisco").validated_save()'
docker exec -it nautobot nautobot-server nbshell --command 'DeviceType(manufacturer=Manufacturer.objects.get(slug="cisco"),model="Nexus 9000v",slug="nexus9000v").validated_save()'
docker exec -it nautobot nautobot-server nbshell --command 'DeviceType(manufacturer=Manufacturer.objects.get(slug="cisco"),model="CSR 1000v",slug="csr1000v").validated_save()'
docker exec -it nautobot nautobot-server nbshell --command 'DeviceRole(name="Router", slug="router").validated_save()'
docker exec -it nautobot nautobot-server nbshell --command 'DeviceRole(name="Switch", slug="switch").validated_save()'
docker exec -it nautobot nautobot-server nbshell --command 'Platform(name="Cisco NX-OS", slug="cisco_nxos",manufacturer=Manufacturer.objects.get(slug="cisco")).validated_save()'
docker exec -it nautobot nautobot-server nbshell --command 'Platform(name="Cisco IOS", slug="cisco_ios",manufacturer=Manufacturer.objects.get(slug="cisco")).validated_save()'

# create superuser
docker exec -it nautobot nautobot-server createsuperuser
