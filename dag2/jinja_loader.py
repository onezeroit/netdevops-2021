'''
Jinja loader utility

importeer via:
from jinja_loader import render_template

gebruik met:
output = render_template(template_filename, [yaml_files])
'''

from jinja2 import Environment, FileSystemLoader, TemplateNotFound
import yaml
import os


def jinja_render(template, variables):
    template_dir = os.path.dirname(template)
    template_file = os.path.basename(template)
    j2_env = Environment(loader=FileSystemLoader(template_dir))
    try:
        output = j2_env.get_template(template_file).render(**variables)
        return output
    except TemplateNotFound:
        print("Template niet gevonden - controleer het pad")


def load_yaml_files(files):
    output = dict()
    if not isinstance(files, list):
        file_list = [files]
    else:
        file_list = files
    for vars_file in file_list:
        try:
            with open(vars_file, 'r') as f:
                variables = yaml.safe_load(f)
        except FileNotFoundError:
            print("variabelen files niet gevonden - controleer het pad")
        # combineer variabelen
        output.update(variables)
    return output


def render_template(template_file,var_files):
    variables = load_yaml_files(var_files)
    output = jinja_render(template_file,variables)
    return output


if __name__ == '__main__':
    # TESTCODE
    file1 = 'oefening2_vars_common.yml'
    file2 = 'oefening2_vars_RTR1.yml'
    template = 'oefening2.j2'
    var_files = [file1,file2]
    print(render_template(template,var_files))
