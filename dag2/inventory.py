cisco_user = "cisco"
cisco_pass = "cisco"

devices = {
    "RTR1": {
        "host": "10.10.20.175",
        "username": cisco_user,
        "password": cisco_pass,
        "device_type": "autodetect",
    },
    "RTR2": {
        "host": "10.10.20.176",
        "username": cisco_user,
        "password": cisco_pass,
        "device_type": "autodetect",
    },
    "SW01": {
        "host": "10.10.20.177",
        "username": cisco_user,
        "password": cisco_pass,
        "device_type": "autodetect",
    },
    "SW02": {
        "host": "10.10.20.178",
        "username": cisco_user,
        "password": cisco_pass,
        "device_type": "autodetect",
    }
}