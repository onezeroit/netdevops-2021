'''
Maak een script dat een CMDB-lijstje maakt:
1. Haal (met Netmiko) ‘show version’ op uit al je devices
2. Parse dit (met TTP) om er de volgende informatie uit te halen:
    Hostname
    Model
    Serial number
    Software version
3. Print dit in een tabel

'''
from netmiko import Netmiko, SSHDetect
from ttp import ttp

# inventory
import inventory

# variabelen
command = "show version"
templates = {  # verschillende templates voor ios en nxos
    "cisco_ios": "templates/show_version_ios.ttp",
    "cisco_nxos": "templates/show_version_nxos.ttp"
}
results = list()

# loop over routers
for router in inventory.devices.values():

    ## detecteer het juiste device_type
    guesser = SSHDetect(**router)
    device_type = guesser.autodetect()
    print(router['host']+": device_type is waarschijnlijk "+device_type)  # Name of the best device_type to use further
    # Update the 'device' dictionary with the device_type
    router["device_type"] = device_type

    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    output = net_connect.send_command(command)
    # disconnect
    net_connect.disconnect()

    # parse version data
    template = templates[device_type] # kies juiste template (ios/nxos)
    parser = ttp(output, template)    # create parser object
    parser.parse()                    # parse data
   
    # sla resultaten op
    results.append(parser.result()[0][0])

# print resultaten
print(f"{'hostname':<20}{'model':<20}{'serial':<20}{'version':<20}")
for result in results:
    print(f"{result['hostname']:<20}{result['model']:<20}{result['serial']:<20}{result['version']:<20}")