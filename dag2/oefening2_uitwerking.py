'''
Oefening 2: Jinja2 en YAML

Importeer variabelen uit YAML en render de config voor RTR1
'''

from jinja_loader import jinja_render
import yaml

# variabelen
template_file = 'templates/oefening2.j2'
common_vars_file = 'oefening2_vars_common.yml'
rtr1_vars_file = 'oefening2_vars_RTR1.yml'
rendered_config =  'output/RTR1.config'

# lees variabelen in
with open(common_vars_file, 'r') as f:
    common_vars = yaml.safe_load(f)

with open(rtr1_vars_file, 'r') as f:
    rtr1_vars = yaml.safe_load(f)

# combineer variabelen
context = dict()
context.update(common_vars)
context.update(rtr1_vars)

# render template
output = jinja_render(template_file, context)

with open(rendered_config, 'w') as f:
    f.write(output)
