"""
Maak een script dat een interface descriptions zet op basis van CDP neighbors:

1. Haal (met Netmiko) CDP informatie op uit al je devices
2. Parse dit (met TTP) 
3. Gebruik een Jinja template om hiervan interface configuraties te maken
4. Push deze configuratie (met Netmiko) naar je devices

"""
from netmiko import Netmiko, SSHDetect
from ttp import ttp
from jinja_loader import jinja_render

# inventory
import inventory

# variabelen
command = "show cdp neighbors detail"
templates = {
    "cisco_ios": "templates/show_cdp_ios.ttp",
    "cisco_nxos": "templates/show_cdp_nxos.ttp"
}
j2_template = "templates/oefening4.j2"

for router in inventory.devices.values():

    ## detecteer het juiste device_type
    guesser = SSHDetect(**router)
    device_type = guesser.autodetect()
    print(router['host']+": device_type is waarschijnlijk "+device_type)  # Name of the best device_type to use further
    # Update the 'device' dictionary with the device_type
    router["device_type"] = device_type

    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    output = net_connect.send_command(command)

    # parse cdp data
    ttp_template = templates[device_type] # kies IOS of NXOS template
    parser = ttp(output, ttp_template)    # create parser object
    parser.parse()                        # parse data
    neighbors = parser.result()[0][0]     # sla neigbor data op in een variabele

    # gebruik neighbor variabele om config te renderen
    config = jinja_render(j2_template, neighbors)
    
    # schrijf rendered config naar bestand
    config_file = "./output/"+router['host']+".config"
    with open(config_file, 'w') as f:
        f.write(config)

    # wijzig config
    changelog = net_connect.send_config_from_file(config_file)
    changelog += net_connect.save_config()

    # disconnect
    net_connect.disconnect()

    print(changelog)
