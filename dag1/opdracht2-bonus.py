'''
Bonus opdracht

Maak een config backup van beide routers (RTR1, RTR2) en beide switches (SW01,SW02)
    - gebruik autodiscovery om het device type te vinden
‘show run’
Sla op als <hostname>-<datum>.config

'''
from netmiko import Netmiko, SSHDetect
from datetime import date

# inventory
cisco_user = "cisco"
cisco_pass = "cisco"

RTR1 = {
    "host": "10.10.20.175",
    "username": cisco_user,
    "password": cisco_pass,
    "device_type": "autodetect",
}
RTR2 = {
    "host": "10.10.20.176",
    "username": cisco_user,
    "password": cisco_pass,
    "device_type": "autodetect",
}
SW01 = {
    "host": "10.10.20.177",
    "username": cisco_user,
    "password": cisco_pass,
    "device_type": "autodetect",
}
SW02 = {
    "host": "10.10.20.178",
    "username": cisco_user,
    "password": cisco_pass,
    "device_type": "autodetect",
}

# set date string
today = str(date.today())

# backup command
command = "show run"

# loop over routers
for router in [RTR1,RTR2,SW01,SW02]:
    ## detecteer het juiste device_type
    guesser = SSHDetect(**router)
    best_match = guesser.autodetect()
    print(router['host']+": device_type is waarschijnlijk "+best_match)  # Name of the best device_type to use further
    # Update the 'device' dictionary with the device_type
    router["device_type"] = best_match

    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    output = net_connect.send_command(command)
    # disconnect
    net_connect.disconnect()

    # save config
    filename = router['host']+"-"+today+".config"
    print(router['host']+": saving config to "+filename)
    with open(filename, "w") as output_file:
        output_file.write(output)
