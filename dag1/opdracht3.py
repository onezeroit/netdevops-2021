'''
Wijzig config van beide routers (RTR1, RTR2)
  Login banner, snmp
  Save config
'''

from netmiko import Netmiko

## inventory
cisco_user = "cisco"
cisco_pass = "cisco"

RTR1 = {
    "host": "10.10.20.175",
    "username": cisco_user,
    "password": cisco_pass,
    "device_type": "cisco_ios",
}
RTR2 = {
    "host": "10.10.20.176",
    "username": cisco_user,
    "password": cisco_pass,
    "device_type": "cisco_ios",
}

# banner
banner = "UNAUTHORIZED ACCESS TO THIS DEVICE IS PROHIBITED\nYou must have explicit, authorized permission to access or configure this device.\nUnauthorized attempts and actions to access or use this system may result in civil and/or criminal penalties.\nAll activities performed on this device are logged and monitored.\n"

# config commands
commands = [
    "banner login #"+banner+"#",
    "snmp-server community openbaar RO",
]

# loop over routers
for router in [RTR1,RTR2]:
    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # send config
    print(router['host']+": sending config")
    net_connect.send_config_set(commands)

    # save config
    print(router['host']+": copy run start")
    output = net_connect.save_config()
    print(output)

    # disconnect
    net_connect.disconnect()
