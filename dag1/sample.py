from netmiko import Netmiko

RTR1 = {
    "host": "10.10.20.175",
    "username": "cisco",
    "password": "cisco",
    "device_type": "cisco_ios",
}

net_connect = Netmiko(**RTR1)
command = "show ip int brief"

print()
print(net_connect.find_prompt())
output = net_connect.send_command(command)
net_connect.disconnect()
print(output)
print()
